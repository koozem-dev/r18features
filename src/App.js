import './App.css';
import { useDeferredValue, useState, useTransition } from "react";
import SearchResults from "./SearchResults";

function App() {
  const [query, setQuery] = useState('');
  const [filters, setFilters] = useState('a');
  const [isPending, startTransition] = useTransition();

  // даем время одуматься и ввести все, что нужно, перед тем, как выводить результаты
  const deferredQuery = useDeferredValue(query);

  const selectFilters = (newFilters) => {
    // даем время одуматься и поменять выбор, перед тем, как выводить результаты
    startTransition(() => {
      setFilters(newFilters);
    });
  }

  return (
    <div className="App">
    <label>
      Search something:
      <input value={query} onChange={e => setQuery(e.target.value)} />
    </label>
      <label>
        Filter 'A':
        <input type='radio' defaultChecked name="group1" value='a' onChange={() => selectFilters('a')} />
      </label>
      <label>
        Filter 'B':
        <input type='radio' name="group1" value='b' onChange={() => selectFilters('b')} />
      </label>
      <label>
        Filter 'C':
        <input type='radio' name="group1" value='c' onChange={() => selectFilters('c')} />
      </label>
      {isPending && <p>Одумайся!</p>}
      <SearchResults query={deferredQuery} filter={filters} />
    </div>
  );
}

export default App;
