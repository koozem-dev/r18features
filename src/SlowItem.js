
function SlowItem({ text }) {

  // еще вот тут чтоб задержка была
  let startTime = performance.now();
  while (performance.now() - startTime < 10) {
    // тут например делается какая-то сложная логика и нужно время на формирование результата
  }

  return (
    <div className="Slow-search">
      {text}
    </div>
  );
}

export default SlowItem;
