import SlowItem from "./SlowItem";
import {memo} from "react";

const someData = [
  'aaa', 'bb', 'cdf', 'sdf', 'rrr', 'pooiutqwqer', 'lkjhmnjkh', 'gbvcxz',
  'aaa', 'bab', 'cdaf', 'sdfaaaaa', 'rrar', 'pooiutqwqer', 'lkjhmaanjkh', 'gbvacxz',
  'aaa', 'baaab', 'cdaaf', 'sdssaaaaf', 'rraaaaaaaaaar', 'pooaaaaaaaiutqwqer', 'lkjhaaamnjkh', 'gbvcaaaaxz',
  'aaa', 'bab', 'cdaf', 'sdfaaaaa', 'rrar', 'pooiutqwqer', 'lkjhmaanjkh', 'gbvacxz',
  'aaa', 'baaab', 'cdaaf', 'sdssaaaaf', 'rraaaaaaaaaar', 'pooaaaaaaaiutqwqer', 'lkjhaaamnjkh', 'gbvcaaaaxz',
  'aaa', 'bab', 'cdaf', 'sdfaaaaa', 'rrar', 'pooiutqwqer', 'lkjhmaanjkh', 'gbvacxz',
  'aaa', 'baaab', 'cdaaf', 'sdssaaaaf', 'rraaaaaaaaaar', 'pooaaaaaaaiutqwqer', 'lkjhaaamnjkh', 'gbvcaaaaxz',
  'aaa', 'bab', 'cdaf', 'sdfaaaaa', 'rrar', 'pooiutqwqer', 'lkjhmaanjkh', 'gbvacxz',
  'aaa', 'baaab', 'cdaaf', 'sdssaaaaf', 'rraaaaaaaaaar', 'pooaaaaaaaiutqwqer', 'lkjhaaamnjkh', 'gbvcaaaaxz'
];

function SearchResults({ query, filter }) {
  let items = [];
  const results = someData.filter((value) => value.includes(query));

  let filteredResult = [];
  // for (const filterKey in filter) {
  //   if (filter[filterKey]) {
  //     const letter = filterKey === 'useA' ? 'a' : (filterKey === 'useB' ? 'b' : 'c');
      filteredResult.push(...results.filter((value) => value.includes(filter)));
    // }
  // }
  // ну шоб точно подольше
  results.forEach((value) => value + 'find');
  results.forEach((value) => value);
  items.push(filteredResult.map((result, index) => <SlowItem key={index} text={result}/>));

  return (
    <div
      className="Search-results"
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      {items}
    </div>
  );
}

export default memo(SearchResults);
